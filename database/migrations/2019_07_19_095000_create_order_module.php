<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Uccello\Core\Database\Migrations\Migration;
use Uccello\Core\Models\Module;
use Uccello\Core\Models\Domain;
use Uccello\Core\Models\Tab;
use Uccello\Core\Models\Block;
use Uccello\Core\Models\Field;
use Uccello\Core\Models\Filter;
use Uccello\Core\Models\RelatedList;
use Uccello\Core\Models\Link;

class CreateOrderModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->createTable();
        $module = $this->createModule();
        $this->activateModuleOnDomains($module);
        $this->createTabsBlocksFields($module);
        $this->createFilters($module);
        $this->createRelatedLists($module);
        $this->createLinks($module);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Drop table
        Schema::dropIfExists($this->tablePrefix . 'orders');

        // Delete module
        Module::where('name', 'order')->forceDelete();
    }

    protected function initTablePrefix()
    {
        $this->tablePrefix = '';

        return $this->tablePrefix;
    }

    protected function createTable()
    {
        Schema::create($this->tablePrefix . 'orders', function (Blueprint $table) {
            $table->increments('id');
            $table->text('order');
            $table->boolean('paid')->nullable();
            $table->text('comment')->nullable();
            $table->boolean('ready')->nullable();
            $table->boolean('sent')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    protected function createModule()
    {
        $module = new Module([
            'name' => 'order',
            'icon' => 'compare_arrows',
            'model_class' => 'App\Order',
            'data' => null
        ]);
        $module->save();
        return $module;
    }

    protected function activateModuleOnDomains($module)
    {
        $domains = Domain::all();
        foreach ($domains as $domain) {
            $domain->modules()->attach($module);
        }
    }

    protected function createTabsBlocksFields($module)
    {
        // Tab tab.main
        $tab = new Tab([
            'module_id' => $module->id,
            'label' => 'tab.main',
            'icon' => null,
            'sequence' => 0,
            'data' => null
        ]);
        $tab->save();

        // Block block.admin
        $block = new Block([
            'module_id' => $module->id,
            'tab_id' => $tab->id,
            'label' => 'block.admin',
            'icon' => 'supervisor_account',
            'sequence' => 0,
            'data' => json_decode('{"description":"block.admin.description"}')
        ]);
        $block->save();

        // Field order
        $field = new Field([
            'module_id' => $module->id,
            'block_id' => $block->id,
            'name' => 'order',
            'uitype_id' => uitype('textarea')->id,
            'displaytype_id' => displaytype('everywhere')->id,
            'sequence' => 0,
            'data' => json_decode('{"rules":"required","large":true}')
        ]);
        $field->save();

        // Field paid
        $field = new Field([
            'module_id' => $module->id,
            'block_id' => $block->id,
            'name' => 'paid',
            'uitype_id' => uitype('boolean')->id,
            'displaytype_id' => displaytype('everywhere')->id,
            'sequence' => 1,
            'data' => null
        ]);
        $field->save();

        // Field updated_at
        $field = new Field([
            'module_id' => $module->id,
            'block_id' => $block->id,
            'name' => 'updated_at',
            'uitype_id' => uitype('datetime')->id,
            'displaytype_id' => displaytype('detail')->id,
            'sequence' => 2,
            'data' => null
        ]);
        $field->save();

        // Block block.atelier
        $block = new Block([
            'module_id' => $module->id,
            'tab_id' => $tab->id,
            'label' => 'block.atelier',
            'icon' => 'local_shipping',
            'sequence' => 1,
            'data' => json_decode('{"description":"block.atelier.description"}')
        ]);
        $block->save();

        // Field comment
        $field = new Field([
            'module_id' => $module->id,
            'block_id' => $block->id,
            'name' => 'comment',
            'uitype_id' => uitype('textarea')->id,
            'displaytype_id' => displaytype('everywhere')->id,
            'sequence' => 0,
            'data' => json_decode('{"large":true}')
        ]);
        $field->save();

        // Field ready
        $field = new Field([
            'module_id' => $module->id,
            'block_id' => $block->id,
            'name' => 'ready',
            'uitype_id' => uitype('boolean')->id,
            'displaytype_id' => displaytype('everywhere')->id,
            'sequence' => 1,
            'data' => null
        ]);
        $field->save();

        // Field sent
        $field = new Field([
            'module_id' => $module->id,
            'block_id' => $block->id,
            'name' => 'sent',
            'uitype_id' => uitype('boolean')->id,
            'displaytype_id' => displaytype('everywhere')->id,
            'sequence' => 2,
            'data' => null
        ]);
        $field->save();

    }

    protected function createFilters($module)
    {
        // Filter
        $filter = new Filter([
            'module_id' => $module->id,
            'domain_id' => null,
            'user_id' => null,
            'name' => 'filter.all',
            'type' => 'list',
            'columns' => [ 'order', 'comment', 'updated_at', 'ready', 'sent', 'paid' ],
            'conditions' => null,
            'order_by' => null,
            'is_default' => true,
            'is_public' => false,
            'data' => [ 'readonly' => true ]
        ]);
        $filter->save();

    }

    protected function createRelatedLists($module)
    {
    }

    protected function createLinks($module)
    {
    }
}