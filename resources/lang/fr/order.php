<?php

return [
    'order' => 'Commandes',
    'single.order' => 'Commande',
    'tab.main' => 'Principal',
    'block.admin' => 'Reservé administration',
    'block.admin.description' => 'Espace reservé à l\'administration',
    'block.atelier' => 'Reservé atelier',
    'block.atelier.description' => 'Espace réservé à l\'atelier',

    'field' => [
        'comment' => 'Commentaire',
        'order' => 'Commande',
        'ready' => 'A préparer',
        'sent' => 'A expédier',
        'paid' => 'Facturé',
        'updated_at' => 'Dernière modification',
    ],
];