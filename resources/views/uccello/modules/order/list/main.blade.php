@extends('uccello::modules.default.list.main')

@section('sidebar-left')
@overwrite

@section('extra-script')
    {!! Html::script(mix('js/order.js')) !!}
@append
